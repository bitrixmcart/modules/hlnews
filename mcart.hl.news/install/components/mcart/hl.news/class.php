<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Errorable;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;

use Bitrix\Main\Data\Cache;
use Bitrix\Main\Data\TaggedCache;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

use Mcart\Hl\News\Helper;
use Mcart\Hl\News\HlBlocks\HlBlock as MHighLoadBlock;

use Bitrix\Main\Entity\Query;
use Bitrix\Highloadblock\HighloadBlockTable;

/**
 * Контроллер работы с новостями
 * 
 * Выполняет обработку и вывод списка новостей
 *
 */
class McartHlNews extends CBitrixComponent implements Errorable
{
    private ErrorCollection $errorCollection;

    private Cache $cache;
    private TaggedCache $taggedCache;

    private string $cachePath;
    private string $cacheKey;
    private bool $cacheInvalid;

    
    public function getErrors(): array {
        return $this->errorCollection->toArray();
    }

    public function getErrorByCode($code): Error {
        return $this->errorCollection->getErrorByCode($code);
    }

    private function addError(Error $error): void {
        $this->errorCollection->setError($error);
    }
    
    private function addErrors(array $errors): void {
        $this->errorCollection->add($errors);
    }

    private function showErrors(): bool {
        if (count($this->getErrors())) {
            foreach ($this->getErrors() as $error) {
                if ((int)$error->getCode() === 404) {
                    ShowError($error->getMessage());
                }
            }
            return true;
        }
        return false;
    }


    public function onPrepareComponentParams($arParams) {
        $this->errorCollection = new ErrorCollection();

        if (! Loader::IncludeModule("highloadblock")) {
            $this->addError(
                new Error(Loc::getMessage('MCART_MAIN_PAGE_BANNER_MODULE_NOT_INSTALLED', ["#MODULE#" => "highloadblock"]), 404)
            );
            return false;
        }

        if (! Loader::IncludeModule("mcart.hl.news")) {
            $this->addError(
                new Error(Loc::getMessage('MCART_MAIN_PAGE_BANNER_MODULE_NOT_INSTALLED', ["#MODULE#" => "mcart.hl.news"]), 404)
            );
            return false;
        }

        // cоздать экземпляр класса для работы с кэшем 
        $this->cache = Cache::createInstance();
        // название директории для кэша, /bitrix/cache/
        $this->cachePath = self::class;
        // уникальный ключ для названия файла кэша
        $this->cacheKey = self::class . "_" . md5(json_encode($_REQUEST));
        $this->cacheInvalid = false;
        // объект тегированного кеша, информация по тегированному кешу  в b_cache_tag
        $this->taggedCache = Application::getInstance()->getTaggedCache();

        if (! isset($arParams["CACHE_TIME"])) {
            $arParams["CACHE_TIME"] = 36000000;
        }
        if (! isset($arParams["HL_IBLOCK_ID"])) {
            $arParams["HL_IBLOCK_ID"] = Option::get(Helper::getModuleId(), "hl_" . MHighLoadBlock::getHlBlockName());
        }
        if (! isset($arParams["ELEMENTS_COUNT"])) {
            $arParams["ELEMENTS_COUNT"] = Option::get(Helper::getModuleId(), "newsPageCnt", "10");
        }

        return parent::onPrepareComponentParams($arParams);
    }

    public function executeComponent(): void {
        if ($this->showErrors()) {
            return;
        }

        // проверить на наличие и актуальность кэша
        if ($this->cache->initCache($this->arParams['CACHE_TIME'], $this->cacheKey, $this->cachePath)) {
            // получить переменные из кеша
            $this->arResult = $this->cache->getVars();
        // начать процесс кеширования
        } elseif ($this->cache->startDataCache()) {
            // начать записывать теги,
            // чтобы тегированный кеш нашел что ему сбрасывать, необходим
            // одинаковый путь в $cache->initCache() и  $taggedCache->startTagCache()
            $this->taggedCache->startTagCache($this->cachePath);

            $this->arResult = array();
            $this->arResult['NEWS'] = $this->getNews();

            // добавить теги
            $this->taggedCache->registerTag(Helper::getCacheSuffixId() . $this->arParams["HL_IBLOCK_ID"] );
            if ($this->cacheInvalid) {
                // остановить кеширование
                $this->taggedCache->abortTagCache();
                $this->cache->abortDataCache();
            }
            // записать данные в кеш
            $this->taggedCache->endTagCache();
            $this->cache->endDataCache($this->arResult);
        }

        $this->includeComponentTemplate();
    }

    /**
     * Функция получения экземпляра класса
     *
     * @param string $hlBlockId Идентификатор HL-блока
     */
    private function getEntityDataClassById($hlBlockId) {
        if (empty($hlBlockId) || (intval($hlBlockId) < 1)) {
            return false;
        }
        
        // выборка информации о сущности из базы данных
        $hlblock = HighloadBlockTable::getById($hlBlockId)->fetch();
        // инициализация класса сущности
        $entity = HighloadBlockTable::compileEntity($hlblock);

        return $entity->getDataClass();
    }

    /**
     * Получить список новостей
     *
     * Сбор информации по новостям
     * 
     * @return array
     */
    private function getNews(): array {
        $items = [];

        $navigationId = "NAV_NEWS";
        $navigation = new \Bitrix\Main\UI\PageNavigation(strtolower($navigationId));
        $navigation->allowAllRecords(true)
            ->setPageSize($this->arParams["ELEMENTS_COUNT"])
            ->initFromUri();

        $entity = $this->getEntityDataClassById($this->arParams["HL_IBLOCK_ID"]);
        $dbQuery = new Query($entity);
        $dbQuery->setSelect(array(
            "ID",
            "UF_NAME",
            "FULL_NAME",
        ));
        $dbQuery->setOrder(array(
            "ID" => "desc",
        ));
        $dbQuery->registerRuntimeField(
            "USER",
            array(
                "data_type" => "Bitrix\Main\UserTable",
                "reference" => array("=this.UF_AUTHOR" => "ref.ID"),
            )
        );
        $dbQuery->registerRuntimeField(
            new \Bitrix\Main\Entity\ExpressionField(
                "FULL_NAME", 'CONCAT(%s, " ", %s, " ",%s)', array("USER.LAST_NAME", "USER.NAME", "USER.SECOND_NAME")
            )
        );
        $dbQuery->whereIn("UF_ACTIVE", true);

        $dbQuery->countTotal(1)
            ->setOffset($navigation->getOffset())
            ->setLimit($navigation->getLimit());

        $queryCollection = $dbQuery->exec();

        $navigation->setRecordCount($queryCollection->getCount());
        $this->arResult[$navigationId] = $navigation;

        while ($obQuery = $queryCollection->fetchObject()) {
            $key = $obQuery->getId();
            $items[$key] = array(
                "NAME" => $obQuery->get("UF_NAME"),
                "AUTHOR" => $obQuery->get("FULL_NAME"),
            );
        }

        return $items;
    }
}