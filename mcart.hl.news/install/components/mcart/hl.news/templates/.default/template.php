<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
?>
<div>
    <table class="table__news">
        <thead>
            <tr>
                <th><?=  Loc::getMessage("MCART_HL_NEWS_TH_NAME")?></th>
                <th><?=  Loc::getMessage("MCART_HL_NEWS_TH_AUTHOR")?></th>
            </tr>
        </thead>
        <tbody>
                <?php
                    if (is_array($arResult["NEWS"])) {
                        foreach ($arResult["NEWS"] as $item) {
                ?>
                    <tr>
                        <td><?= $item["NAME"]?></td>
                        <td><?= $item["AUTHOR"]?></td>
                    </tr>
                <?php
                        }
                    }
                ?>
        </tbody>
    </table>
</div>
<div>
    <?php
        $APPLICATION->IncludeComponent("bitrix:main.pagenavigation", "", array(
            "NAV_OBJECT" => $arResult["NAV_NEWS"],
        ));
    ?>
</div>