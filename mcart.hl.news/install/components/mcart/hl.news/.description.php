<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
"NAME" => Loc::getMessage("MCART_HL_NEWS_NAME"),
"DESCRIPTION" => Loc::getMessage("MCART_HL_NEWS_DESCRIPTION"),
"PATH" => array(
	"ID" => "content",
	"CHILD" => array(
		"ID" => "vcart_hl_news",
		"NAME" =>  Loc::getMessage("MCART_HL_NEWS_LIST"),
		)
	),
);
