<?php
use Bitrix\Main\Localization\Loc;

if (! check_bitrix_sessid()) return;

if ($ex = $APPLICATION->GetException()) {
	echo CAdminMessage::ShowMessage(array(
		'TYPE' => 'ERROR',
		'MESSAGE' => Loc::getMessage('MOD_INST_ERR'),
		'DETAILS' => $ex->GetString(),
		'HTML' => true,
	));
?>
    <form action="<?= $APPLICATION->GetCurPage()?>">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="lang" value="<?= LANG?>">
        <input type="submit" name="" value="<?= Loc::getMessage("MOD_BACK")?>">
    </form>
<?php
} else {
?>
    <form action="<?= $APPLICATION->GetCurPage()?>" name="form1">
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="lang" value="<?= LANG?>">
        <input type="hidden" name="id" value="mcart.hl.news">
        <input type="hidden" name="install" value="Y">
        <input type="hidden" name="step" value="2">
        <input type="submit" name="inst" value="<?= Loc::getMessage("MOD_INSTALL")?>">
    </form>
<?}?>
