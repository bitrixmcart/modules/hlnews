<?php

use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

use Mcart\Hl\News\HlBlocks\Hlblock as MHighLoadBlock;
use Mcart\Hl\News\DbTabls\UsercntnewsTable;

Loc::loadMessages(__FILE__);

class Mcart_Hl_News extends CModule
{

    public function __construct() {
        $this->MODULE_ID = "mcart.hl.news"; 
        
        $arModuleVersion = array();
		include(__DIR__.'/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->MODULE_NAME = Loc::getMessage('MHLN_INSTALL_NAME');  
		$this->MODULE_DESCRIPTION = Loc::getMessage('MHLN_INSTALL_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('MHLN_INSTALL_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('MHLN_INSTALL_PARTNER_URL');

        $this->MODULE_GROUP_RIGHTS = 'Y'; 
    }

    protected function getPath(bool $notDocumentRoot = false): string {
        $path = dirname(__DIR__);
        $path = str_replace('\\', '/', $path);
        
        return ($notDocumentRoot) ? preg_replace("#^(.*)\/(local|bitrix)\/modules#", "$2/modules", $path) : $path; 
    }

    public function doInstall(): void {
        global $APPLICATION, $step;

        if (ModuleManager::isModuleInstalled($this->MODULE_ID)) return;

        $step = IntVal($step);
        if($step < 2)
            $APPLICATION->IncludeAdminFile(Loc::getMessage('MHLN_INSTALL_TITLE'), $this->getPath() . '/install/step1.php');
        elseif($step == 2) {
            ModuleManager::RegisterModule($this->MODULE_ID); 

            $this->installDB();
            $this->installHLBlock();
            $this->InstallEvents();
            $this->installFiles();

            $APPLICATION->IncludeAdminFile(Loc::getMessage('MHLN_INSTALL_TITLE'), $this->getPath() . '/install/step2.php');
        }
    }

    public function doUninstall(): void {
        global $APPLICATION, $step;

        $step = IntVal($step);
        if($step < 2)
            $APPLICATION->IncludeAdminFile(Loc::getMessage('MHLN_UNINSTALL_TITLE'), $this->getPath() . '/install/unstep1.php');
        elseif($step == 2) {

            if (! (array_key_exists('savedata', $_REQUEST) && ($_REQUEST['savedata'] == 'Y'))) {
                $this->unInstallHLBlock(); 
                $this->unInstallDB(); 
            }		

            $this->UnInstallEvents();
            $this->UnInstallFiles();

            ModuleManager::UnRegisterModule($this->MODULE_ID); 
            $APPLICATION->IncludeAdminFile(Loc::getMessage('MHLN_UNINSTALL_TITLE'), $this->getPath() . '/install/unstep2.php');
        }
    }

    public function installDB(): bool {
        if (! Loader::includeModule($this->MODULE_ID)) {
            return false;
        }

        return UsercntnewsTable::createTable();
    }

    public function unInstallDB(): bool {
        if (! Loader::includeModule($this->MODULE_ID)) {
            return false;
        }

        return UsercntnewsTable::dropTable();
    }

    public function installHlBlock(): void {
        if (Loader::includeModule($this->MODULE_ID)) {
            MHighLoadBlock::setUp();
        }
    }

    public function unInstallHlBlock(): void {
        if (Loader::includeModule($this->MODULE_ID)) {
            MHighLoadBlock::setDown();
        }
    }

    public function installFiles(): void {
        $docRoot = Application::getDocumentRoot();
        CopyDirFiles(
			$docRoot."/local/modules/".$this->MODULE_ID."/install/components",
			$docRoot."/local/components", true, true
		);
    }
    
    public function unInstallFiles(): void {
        $docRoot = Application::getDocumentRoot();
        $folders = array(
            $docRoot."/local/components/mcart/"."hl.news",
        );

        foreach ($folders as $folder) {
            Directory::deleteDirectory($folder);
        }
	}

    function InstallEvents(): void {
        $eventManager = EventManager::getInstance(); 
        $eventManager->registerEventHandler("", MHighLoadBlock::getHlBlockName() . "OnAfterAdd",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterAddHl");
        $eventManager->registerEventHandler("", MHighLoadBlock::getHlBlockName() . "OnAfterUpdate",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterUpdateHl");
        $eventManager->registerEventHandler("", MHighLoadBlock::getHlBlockName() . "OnAfterDelete",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterDeleteHl");
	}

    function UnInstallEvents(): void {
        if (! Loader::includeModule($this->MODULE_ID)) {
            return;
        }

        $eventManager = EventManager::getInstance(); 
        // $eventManager->unRegisterEventHandler("", "NewsOnAfterAdd",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterAddHl");
        // $eventManager->unRegisterEventHandler("", "NewsOnAfterUpdate",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterUpdateHl");
        // $eventManager->unRegisterEventHandler("", "NewsOnAfterDelete",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterDeleteHl");
        $eventManager->unRegisterEventHandler("", MHighLoadBlock::getHlBlockName() . "OnAfterAdd",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterAddHl");
        $eventManager->unRegisterEventHandler("", MHighLoadBlock::getHlBlockName() . "OnAfterUpdate",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterUpdateHl");
        $eventManager->unRegisterEventHandler("", MHighLoadBlock::getHlBlockName() . "OnAfterDelete",  $this->MODULE_ID, "\\Mcart\\Hl\\News\\Events\\Event", "onAfterDeleteHl");
	}
}
