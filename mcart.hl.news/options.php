<?php
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\HttpApplication;

global $USER;
if (!$USER->isAdmin()) return false;

$request = HttpApplication::getInstance()->getContext()->getRequest();
$moduleId = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);
if (! Loader::includeModule($moduleId)) return false;

$defaultPageCnt = "10";


$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('MHLN_TAB_EDIT_1_NAME'),
        'TITLE' => Loc::getMessage('MHLN_TAB_EDIT_1_TITLE'),
        'OPTIONS' => array(
            array('newsPageCnt', Loc::getMessage('MHLN_TAB_EDIT_1_FIELD_1_TITLE'), $defaultPageCnt, array('text', 5)),
        )
    ),
);


if($request->isPost() && $request["save"] && check_bitrix_sessid()){
    foreach($aTabs as $aTab){
        if(count($aTab['OPTIONS'])) {
            __AdmSettingsSaveOptions($moduleId, $aTab["OPTIONS"]);
        }
    }
}

$tabControl = new \CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<form method="post" action="<?=$APPLICATION->GetCurPage();?>?mid=<?=htmlspecialcharsbx($request["mid"]);?>&lang=<?=LANGUAGE_ID?>" name="<?=$moduleId;?>">
    <? $tabControl->BeginNextTab(); 
        foreach ($aTabs as $aTab) {
            if ($aTab['OPTIONS']) {
                __AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
            }
            $tabControl->beginNextTab();
        }
        $tabControl->Buttons(array('btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false)); 
        echo bitrix_sessid_post(); 
        $tabControl->End();
    ?>
</form>

