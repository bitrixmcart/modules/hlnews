<?php
namespace Mcart\Hl\News\Events;

use  Mcart\Hl\News\Events\HelperEvent;

/**
 * Класс обработчиков событий
 *
 * @package Mcart\Hl\News\Events
 */
class Event
{
    /**
     * Событие вызывается после добавления элемента HL блока.
     *     
     * @param \Bitrix\Main\Entity\Event $event объект события
     */
    public static function onAfterAddHl(\Bitrix\Main\Entity\Event $event): void {
        HelperEvent::clearCacheHlNews();
        HelperEvent::changeCntHlNews();
    }

    /**
     * Событие вызывается после обновления элемента HL блока.
     *     
     * @param \Bitrix\Main\Entity\Event $event объект события
     */
    public static function onAfterUpdateHl(\Bitrix\Main\Entity\Event $event): void {
        HelperEvent::clearCacheHlNews();
    }

    /**
     * Событие вызывается после удаления элемента HL блока.
     *     
     * @param \Bitrix\Main\Entity\Event $event объект события
     */
    public static function onAfterDeleteHl(\Bitrix\Main\Entity\Event $event): void {
        HelperEvent::clearCacheHlNews();
        HelperEvent::changeCntHlNews(true);
    }
}
