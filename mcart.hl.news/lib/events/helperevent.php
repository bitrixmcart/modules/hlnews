<?php
namespace Mcart\Hl\News\Events;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;

use Mcart\Hl\News\Helper;
use Mcart\Hl\News\HlBlocks\HlBlock as MHighLoadBlock;
use Mcart\Hl\News\DbTabls\UsercntnewsTable;

/**
 * Класс методов обработчиков событий
 *
 * @package Mcart\Hl\News\Events
 */
class HelperEvent
{
    /**
     * Изменяет количество добавленных/удаленных новостей пользователем
     *
     * @param bool $isDelete Флаг операции удаления новости
     */
    public static function changeCntHlNews(bool $isDelete = false): void {
        global $USER;

        $arFields = array();

        $rs = UsercntnewsTable::getList(array(
            "select" => array("ID", "USER_ID", "CNT_ADD", "CNT_DEL",),
            "filter" => array('=USER_ID' => $USER->GetID()),
            "order" => array("ID" => "DESC"), 
            "limit" => 1,  
        ));
        if ($ar = $rs->fetch()) {
            if ($isDelete) {
                $arFields["CNT_DEL"] = $ar["CNT_DEL"] + 1;
            } else {
                $arFields["CNT_ADD"] = $ar["CNT_ADD"] + 1;
            }

            UsercntnewsTable::update($ar["ID"], $arFields); 
        } else {
            $arFields["USER_ID"] =  $USER->GetID();
            if ($isDelete) {
                $arFields["CNT_DEL"] = 1;
            } else {
                $arFields["CNT_ADD"] = 1;
            } 

            UsercntnewsTable::add($arFields); 
        }
    }

    /**
     * Очистка кеша по тегу
     *
     * Очистка кеша по HL-блоку новостей
     */
    public static function clearCacheHlNews(): void {
        $hlBlockId= Option::get(Helper::getModuleId(), "hl_" . MHighLoadBlock::getHlBlockName());

        if ($hlBlockId) {
            $taggedCache = Application::getInstance()->getTaggedCache();
            $taggedCache->clearByTag(Helper::getCacheSuffixId() . $hlBlockId);
        }
    }
}
