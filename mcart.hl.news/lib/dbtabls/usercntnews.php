<?php
namespace Mcart\Hl\News\DbTabls;

use Bitrix\Main\Application;
use Bitrix\Main\Entity\Base;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\Entity;

/**
 * Class UsercntnewsTable
 * 
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> USER_ID int mandatory
 * <li> CNT_ADD int optional
 * <li> CNT_DEL int optional
 * </ul>
 *
 * @package Mcart\Hl\News\DbTabls
 **/
class UsercntnewsTable extends DataManager
{
  /**
   * Returns DB table name for entity.
   *
   * @return string
   */
  public static function getTableName(): string {
    return "mcart_user_cnt_news";
  }


  /**
    * Returns entity map definition.
    *
    * @return array
    */
  public static function getMap(): array {
    return array(
      new Entity\IntegerField("ID", array(
        "primary" => true,
        "autocomplete" => true,
      )),
      new Entity\IntegerField("USER_ID", array(
        "required" => true,
      )),
      new Entity\IntegerField("CNT_ADD", array(
        "nullable" => true,
      )),
      new Entity\IntegerField("CNT_DEL", array(
        "nullable" => true,
      )),
    );
  }

	/**
	 * Create DB table for entity
	 *
	 * @return bool
	 */
  public static function createTable(): bool {
    $connection = Application::getConnection(self::getConnectionName());
    $entity = \Bitrix\Main\Entity\Base::getInstance(self::class);
    $tableName = $entity->getDBTableName();
    if (! $connection->isTableExists($tableName)) {
        $entity->createDbTable();
        return true;
    }
    return false;
  }

  /**
	 * Delete DB table
	 *
	 * @return bool
	 */
  public static function dropTable(): bool {
    $connection = Application::getConnection(self::getConnectionName());
    $entity = Base::getInstance(self::class);
    $tableName = $entity->getDBTableName();
    if ($connection->isTableExists($tableName)) {
        $connection->dropTable($tableName);
        return true;
    }
    return false;
  }

}
