<?php
namespace Mcart\Hl\News;

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

/**
 * Класс Helper
 *
 * @package Mcart\Hl\News
 */
class Helper
{
    /**
     * Получить идентификатор модуля HL-блока новостей
     *
     * @return string 
     */
    public static function getModuleId(): string {
        return "mcart.hl.news";
    }

    /**
     * Получить префикс ключа для кеша HL-блока
     *
     * @return string 
     */
    public static function getCacheSuffixId(): string {
        return "hl_block_id_";
    }

    /**
     * Подключить модуль по его имени
     *
     * @param array $modules Список подключаемых модулей
     */
    public static function initModules(array $modules): void {
        if (count($modules) == 0) return;

        foreach ($modules as $module) {
            if (! Loader::includeModule($module)) {
                throw new SystemException('module ' . $module . ' not installed');
            }
        }
    }

}
