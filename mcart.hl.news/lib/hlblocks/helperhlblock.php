<?php
namespace Mcart\Hl\News\HlBlocks;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Highloadblock\HighloadBlockLangTable;
use Bitrix\Main\SystemException;

/**
 * Класс HelperHlBlock
 *
 * @package Mcart\Hl\News\HlBlocks
 */
class HelperHlBlock
{
    /**
     * Возвращает список элементов
     *
     * @param array $select Массив возвращаемых полей элемента
     * @param array $filter Массив фильтруемых полей элемента
     *
     * @return array
     */
    public static function getHLBlock(
        $select = array(),
        $filter = array()
    ): array {
        $dbResult = HighloadBlockTable::getList(array(
            'select' => $select,
            'filter' => $filter,
        ))
        ->fetch();

        if (isset($dbResult) && (!empty($dbResult)))
        {
            return $dbResult;
        }

        return [];
    }

    /**
     * Удаляет HL-блок
     *
     * @param int $hlBlockId Идентификатор HL-блока
     */
    public static function deleteHLblock(int $hlBlockId): void {
        $dbResult = HighloadBlockTable::delete($hlBlockId);
        if (! $dbResult->isSuccess()) {
            throw new SystemException(implode('; ', $dbResult->getErrorMessages()));
        }
    }

    /**
     * Добавляет HL-блок
     *
     * @param string $nameHLblock Название HL-блока
     */
    public static function addHLblock(string $nameHLblock) {
        $dbResult = HighloadBlockTable::add(array(
            'NAME' => $nameHLblock,
            'TABLE_NAME' => 'hl_' . mb_strtolower($nameHLblock, 'UTF-8'),
        ));

        if (! $dbResult->isSuccess()) {
            throw new SystemException(implode('; ', $dbResult->getErrorMessages()));
        } else {
            $id = $dbResult->getId();
            return $id;
        }

        return false;
    }

    /**
     * Добавляет HL-блок
     *
     * @param string $nameHLblock Название HL-блока
     */
    public static function addLangHLblock(array $data): void {
        HighloadBlockLangTable::add($data);
    }

    
    public static function addUserTypeEntity(array $data): void {
        $obUserField = new \CUserTypeEntity();
        $obUserField->Add($data);
    }
    
}
