<?php
namespace Mcart\Hl\News\HlBlocks;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

use Mcart\Hl\News\Helper;
use Mcart\Hl\News\HlBlocks\HelperHlBlock as HelperHLblock;

/**
 * Класс HlBlock
 *
 * Fields:
 * <ul>
 * <li> UF_ACTIVE boolean
 * <li> UF_NAME string
 * <li> UF_AUTHOR employee Привязка к пользователю
 * </ul>
 * 
 * @package Mcart\Hl\News\HlBlocks
 */
class HlBlock
{
    public static function getHlBlockName(): string {
        return "News";
    }

    /**
     * Создать HL-блок
     *
     */
    public static function setUp(): void {
        Helper::initModules(array("highloadblock"));

        $dbResult = HelperHLblock::getHLBlock(
            array("ID"),
            array("NAME"=> self::getHlBlockName())
        );

        if (! $dbResult) {
            $hlBlockId = HelperHLblock::addHLblock(self::getHlBlockName());

            $siteList = \Bitrix\Main\SiteTable::getList(array(
                "select"=>array("LID")
            ))->fetchAll();

            $arFields = Array(
                "NAME" => Loc::getMessage("MHLN_MIGRATIONS_HLBLOCK_NAME"),
                "ID" => $hlBlockId,
                "SITE_ID" => array_column($siteList, "LID"), 
            );

            HelperHLblock::addLangHLblock($arFields);

            HelperHLblock::addUserTypeEntity(self::getUserFields($hlBlockId, array(
                "FIELD_NAME" => "UF_ACTIVE",
                "USER_TYPE_ID" => "boolean",
                "LABEL" =>  Loc::getMessage("MHLN_FIELD_LABEL_UF_ACTIVE"),
                "SORT" => 100
            ))); 

            HelperHLblock::addUserTypeEntity(self::getUserFields($hlBlockId, array(
                "FIELD_NAME" => "UF_NAME",
                "USER_TYPE_ID" => "string",
                "LABEL" =>  Loc::getMessage("MHLN_FIELD_LABEL_UF_NAME"),
                "SORT" => 200
            )));

            HelperHLblock::addUserTypeEntity(self::getUserFields($hlBlockId, array(
                "FIELD_NAME" => "UF_AUTHOR",
                "USER_TYPE_ID" => "employee",
                "LABEL" =>  Loc::getMessage("MHLN_FIELD_LABEL_UF_USER_ID"),
                "SORT" => 300
            )));

            Option::set(Helper::getModuleId(), "hl_" . self::getHlBlockName(),  $hlBlockId);
            
        } else {
            Option::set(Helper::getModuleId(), "hl_" . self::getHlBlockName(),  $dbResult["ID"]);
        }
    }

    /**
     * Удалить HL-блок
     *
     * Удаляет HL-блок и его опциональные настройки
     */
    public static function setDown(): void {
        Helper::initModules(array("highloadblock"));

        $dbResult = HelperHLblock::getHLBlock(
            array("ID"),
            array("NAME"=> self::getHlBlockName())
        );
       
        if ($dbResult) {
            HelperHLblock::deleteHLblock($dbResult["ID"]);

            Option::delete(Helper::getModuleId());
        }
    }

    /**
     * Описание параметров пользователького поля HL-блока
     *
     */
    protected static function getUserFields(int $hlId, array $data): array {
        $arFields = array(
            'ENTITY_ID' => 'HLBLOCK_' . $hlId,
            'FIELD_NAME' => $data['FIELD_NAME'],
            'USER_TYPE_ID' => $data['USER_TYPE_ID'],
            'XML_ID' => $data['XML_ID'],
            'SORT' => $data['SORT'] ?: 100,
            'MULTIPLE' => $data['MULTIPLE'] ?: 'N',
            'MANDATORY' => $data['MANDATORY'] ?: 'N',
            'SHOW_FILTER' => $data['SHOW_FILTER'] ?: 'N',
            'SHOW_IN_LIST' => $data['SHOW_IN_LIST'] ?: 'Y',
            'EDIT_IN_LIST' => $data['MULTIPLE'] ?: 'Y',
            'IS_SEARCHABLE' => $data['IS_SEARCHABLE'] ?: 'N',
            'EDIT_FORM_LABEL' => array('ru' => $data['LABEL']),
            'LIST_COLUMN_LABEL' => array('ru' => $data['LABEL']),
            'LIST_FILTER_LABEL' => array('ru' => $data['LABEL']),
            'ERROR_MESSAGE' => array('ru' => ''),
            'HELP_MESSAGE' => array('ru' => ''),
        );
        
        switch ($data['USER_TYPE_ID']) {
            case 'boolean':
                $arFields['SETTINGS'] = array(
                    'DEFAULT_VALUE' => 1,
                    'DISPLAY' => 'CHECKBOX',
                    'LABEL' => ['', ''],
                    'LABEL_CHECKBOX'  => '',
                );
                break;
            case 'integer':
                $arFields['SETTINGS'] = array(
                    'DEFAULT_VALUE' => 0,
                    'SIZE' => 20,
                    'MIN_VALUE' => 0,
                    'MAX_VALUE' => 0,
                );
                break;
            case 'string':
                $arFields['SETTINGS'] = array(
                    'DEFAULT_VALUE' => '',
                    'SIZE' => 20,
                    'MIN_LENGTH' => 0,
                    'MAX_LENGTH' => 0,
                    'ROWS' => 1,
                    'REGEXP' => '',
                );
                break;
            default:
                $arFields['SETTINGS'] = array();
        }

        return $arFields;
    }
}
